-- Command to access MariaDB in cmd.
mysql -u root -- Dapat may space.

-- Lists  down the databases inside the DBMS/RDBMS
SHOW DATABASES;

-- Create database 
CREATE DATABASE music_db;

-- Drop database
DROP DATABASE music_db;

-- Select a Database:
USE music_db;

-- Create Tables.
-- Table columns have the following format: [column_name] [data_type] [other_options]
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT, -- auto assign id for the new object
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50),
    address VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE artists (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE albumns (
    id INT NOT NULL AUTO_INCREMENT,
    albumn_title VARCHAR(100) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL, 
    PRIMARY KEY (id),
    CONSTRAINT fk_albumns_artist_id
        FOREIGN KEY (artist_id) REFERENCES artists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE songs (
    id INT NOT NULL AUTO_INCREMENT,
    song_name VARCHAR(50) NOT NULL,
    length TIME NOT NULL, 
    genre VARCHAR(50) NOT NULL,
    albumn_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_songs_albumn_id
        FOREIGN KEY (albumn_id) REFERENCES albumn(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists_songs (
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL, 
    PRIMARY KEY (id),
    CONSTRAINT fk_playlists_songs_playlist_id
        FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,

    CONSTRAINT fk_playlists_songs_song_id
        FOREIGN KEY (song_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);